//
//  GameScene.m
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "GameScene.h"
//#import "DataModel.h"
#import "WayPoint.h"
#import "Wave.h"
#import "DefenseEntity.h"
#import "WaveManager.h"

//@class RootViewController;
@class DataModel;
@implementation GameScene

//syn our properties
@synthesize level = m_tileMapLevel;
@synthesize backgroundLayer = m_backgroundLayer;
@synthesize currentLevel = m_currentLevel;
@synthesize objects = m_objectLayerName;

+(id) scene{
    
    CCScene *scene = [CCScene node];
    GameScene *layer = [GameScene node];
    [scene addChild:layer z:1 tag:GameSceneLayer];
    
    HUDLayer *HUD = [HUDLayer sharedHUD];
    [scene addChild:HUD z:2 tag:GameSceneHUDLayer];
    
    DataModel *m = [DataModel getModel];
    m.gameLayer = layer;
    //m.HUDLayer = HUD;
    
   // Add Pause Layer, HIDE
    
    return scene;
}

-(id) init{
    
    if((self = [super init])){
        
        //CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        WaveManager *_waveManager = [WaveManager getWaveManager];
        [_waveManager initWavesWithLevel:ONE];
        
        self.level = [CCTMXTiledMap tiledMapWithTMXFile:@"TileMap.tmx"];
        self.backgroundLayer = [m_tileMapLevel layerNamed:@"Background"];
        self.backgroundLayer.anchorPoint = ccp(0, 0);
        [self addChild:m_tileMapLevel z:0 tag:GameSceneLevel];
        
        self.objects = @"Objects";
        
        // add waypoints from tile map
        [self addWaypoint];
       // [self addWaves];
        
        // call game logic about every second
        [self schedule:@selector(update:)];
        [self schedule:@selector(gameLogic:) interval:1.0];
        
        //move to level manager singleton?
        self.currentLevel = 0;
        
        //self.position = ccp(-228, -122);
        
        DataModel *m = [DataModel getModel];
       
        
        UIPanGestureRecognizer *gestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanFrom:)] autorelease];
        
        //[m.viewController addGestureRecognizer:gestureRecognizer];
        [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:gestureRecognizer];
        
        m.gestureRecognizer = gestureRecognizer;
        
        m_gameHUD = [HUDLayer sharedHUD];
        
       
       
               
    }
    
    return self;
    
}

//-(void)addWaves {
//	
  //  DataModel *m = [DataModel getModel];
	
//	Wave *wave = nil;
	//wave = [[Wave alloc] initWithEnemy:[BasicSeal enemyActor] spawnRate:1.0 totalEnemies:20];
	//[m.waves addObject:wave];
 //   WaveManager *_waveManager = [WaveManager getWaveManager];
//    wave = [_waveManager getNextWave];
    //_waveManager.waveIndex++;
    
 //   [m.waves addObject:wave];
    
 //   wave = nil;     //dealloc
//}

- (Wave *)getCurrentWave{
	
	//DataModel *m = [DataModel getModel];	
	//Wave * wave = (Wave *) [m.waves objectAtIndex:self.currentLevel];
	
    WaveManager *waveManager = [WaveManager getWaveManager];
    Wave *wave = [waveManager getCurrentWave];
    
	return wave;
}

- (Wave *)getNextWave{
    
    CCLOG(@"Get Next Wave number: %i", self.currentLevel);
	
	//DataModel *m = [DataModel getModel];
	WaveManager *w = [WaveManager getWaveManager];
   
    static int timeSinceLastWaveEnded = 0;
        
    double now = [[NSDate date] timeIntervalSince1970];

    int level = w.waveIndex;
    
	//self.currentLevel++;
	
	//if (self.currentLevel > 1)
	//	self.currentLevel = 0;
    
    Wave *wave = nil;
    if(timeSinceLastWaveEnded == 0  || timeSinceLastWaveEnded >= w.spwanRate){
        
        wave = [w getNextWave];
        timeSinceLastWaveEnded = 0;
            
    }
	
    //Wave * wave = (Wave *) [m.waves objectAtIndex:self.currentLevel];
    
    return wave;
}

-(void)addWaypoint {
	DataModel *m = [DataModel getModel];
	
	CCTMXObjectGroup *objects = [self.level objectGroupNamed:@"Objects"];
	WayPoint *wp = nil;
	
	int spawnPointCounter = 0;
	NSMutableDictionary *spawnPoint;
	while ((spawnPoint = [objects objectNamed:[NSString stringWithFormat:@"Waypoint%d", spawnPointCounter]])) {
		int x = [[spawnPoint valueForKey:@"x"] intValue];
		int y = [[spawnPoint valueForKey:@"y"] intValue];
		
		wp = [WayPoint node];
		wp.position = ccp(x, y);
		[m.waypoints addObject:wp];
		spawnPointCounter++;
	}
	
	NSAssert([m.waypoints count] > 0, @"Waypoint objects missing");
	wp = nil;
}

-(void) addTarget{
    
    CCLOG(@"In Add Target, Wave #: %i", self.currentLevel);
    // TODO:
    // ADD REFERENCE TO CLASS TO AVOID REPEAT METHOD CALLS
    DataModel *m = [DataModel getModel];
    WaveManager *w = [WaveManager getWaveManager];
    
    
    Wave *wave = [w getCurrentWave];;
    
    if(wave != nil){
   // 
   // if(wave.totalEnemies < 0){
  ///      return;
   // }
    
    
    //***************
    EnemyActor *target = nil;
    
    //target = [w addTarget];
    target = [wave getTarget];
        if(target == nil){
        
            w.running = NO;
            return;
        }
    
    
    WayPoint *waypoint = [target getCurrentWaypoint ];
	target.position = waypoint.position;	
	waypoint = [target getNextWaypoint];

    //adding with this tag will return a collection of enemies
    [self addChild:target z:1 tag:GameSceneEnemy];
    
    int moveDuration = target.moveDuration;	
	id actionMove = [CCMoveTo actionWithDuration:moveDuration position:waypoint.position];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(followPath:)];
	[target runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
    //add our target to the array of objects
    target.tag = GameSceneEnemy;
    //[m.targets addObject:target];
    }
}

-(void) followPath:(id)sender{
    
   // CCLOG(@"In Follow Path, Wave #: %i", self.currentLevel);

    
    EnemyActor *enemy = (EnemyActor*)sender;
    WayPoint * waypoint = [enemy getNextWaypoint];
    
	int moveDuration = enemy.moveDuration;
	id actionMove = [CCMoveTo actionWithDuration:moveDuration position:waypoint.position];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(followPath:)];
	[enemy stopAllActions];
	[enemy runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

-(void) gameLogic:(ccTime)dt{
    
    //DataModel *m = [DataModel getModel];
    WaveManager *w = [WaveManager getWaveManager];
    static double lastTimeTargetAdded = 0;
    static double lastTimeWaveAdded = 0;
    double now = [[NSDate date] timeIntervalSince1970];

    
    if(w.running){
        Wave *wave = [w getCurrentWave];
        //Wave *wave = [self getCurrentWave];
    
            
            
        if(lastTimeTargetAdded == 0 || now - lastTimeTargetAdded >= wave.spawnRate) {
            [self addTarget];
            lastTimeTargetAdded = now;
    }
    }else{
     
        if(now - lastTimeWaveAdded >= w.waveWaitTime){
            //[w getNextWave];
            
            [w setCurrentWave:[w getNextWave]];
            
            lastTimeWaveAdded = now;
            w.running = YES;
        }
        
    }
}


- (void)update:(ccTime)dt {
    
    //CCLOG(@"inside gamelogic updated, not frozen yet");
    DataModel *m = [DataModel getModel];
    if(m.isPaused){
        CCLOG(@"IS PAUSED   YES");
    }else{
        CCLOG(@"IS PAUSED    NO!");
    }
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    //[self.objects dealloc];
    self.objects = nil;
    
    //[self.level dealloc];
    self.level = nil;
    
    //[self.backgroundLayer dealloc];
    self.backgroundLayer = nil;
    
    
    
	[super dealloc];
}


- (CGPoint)boundLayerPos:(CGPoint)newPos {
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGPoint retval = newPos;
    retval.x = MIN(retval.x, 0);
    retval.x = MAX(retval.x, -m_tileMapLevel.contentSize.width+winSize.width); 
    retval.y = MIN(0, retval.y);
    retval.y = MAX(-m_tileMapLevel.contentSize.height+winSize.height, retval.y); 
    return retval;
}


// allow us to scroll around the map, repleace with pinch zoom
- (void)handlePanFrom:(UIPanGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {    
        
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
        touchLocation = [self convertToNodeSpace:touchLocation];                
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {    
        
        CGPoint translation = [recognizer translationInView:recognizer.view];
        translation = ccp(translation.x, -translation.y);
        CGPoint newPos = ccpAdd(self.position, translation);
        self.position = [self boundLayerPos:newPos];  
        [recognizer setTranslation:CGPointZero inView:recognizer.view];    
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
		float scrollDuration = 0.2;
		CGPoint velocity = [recognizer velocityInView:recognizer.view];
		CGPoint newPos = ccpAdd(self.position, ccpMult(ccp(velocity.x, velocity.y * -1), scrollDuration));
		newPos = [self boundLayerPos:newPos];
        
		[self stopAllActions];
		CCMoveTo *moveTo = [CCMoveTo actionWithDuration:scrollDuration position:newPos];            
		[self runAction:[CCEaseOut actionWithAction:moveTo rate:1]];            
        
    }        
}

-(CGPoint) tileCoordForPosition:(CGPoint) position{
    
    int x = position.x / self.level.tileSize.width;
    int y = ((self.level.mapSize.height * self.level.tileSize.height) - position.y) / self.level.tileSize.height;
    
    return ccp(x, y);
}

-(void) addDefense:(CGPoint)pos{
    
    DataModel *m = [DataModel getModel];
    DefenseEntity *target = nil;
    CGPoint defenseLocation = [self tileCoordForPosition:pos];
    
    int tileGid = [self.backgroundLayer tileGIDAt:defenseLocation];
    NSDictionary *props = [self.level propertiesForGID:tileGid];
    NSString *type = [props valueForKey:@"buildable"];
    
    NSLog(@"Buildable: %@", type);
    
    // ** NEED LOGIC TO DETERMINE TYPE OF STRUCTURE ** TODO
    if([type isEqualToString:@"1"]){
        target = [IceTower tower];
       target.position = ccp( (defenseLocation.x * 32) + 16, self.level.contentSize.height - (defenseLocation.y * 32) - 16);
		[self addChild:target z:1];
        
        target.tag = GameSceneDefense;
        [m.defenseUnits addObject:target];

    }
    
}

-(BOOL) canBuildOnTilePosition:(CGPoint)pos{
    
    CGPoint towerLoc = [self tileCoordForPosition: pos];
	
	int tileGid = [self.backgroundLayer tileGIDAt:towerLoc];
	NSDictionary *props = [self.level propertiesForGID:tileGid];
	NSString *type = [props valueForKey:@"buildable"];
	
	if([type isEqualToString: @"1"]) {
		return YES;
	}
	
	return NO;
}



@end






















