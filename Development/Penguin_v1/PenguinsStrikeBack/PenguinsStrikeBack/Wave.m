//
//  Wave.m
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "Wave.h"

@class EnemyActor;

@implementation Wave


@synthesize spawnRate = m_spawnRate;
@synthesize totalEnemies = m_totalEnemies;
@synthesize enemyIndex  = m_currentEnemyIndex;

@synthesize enemyType = m_enemyType;
@synthesize enemies = m_enemies;


+(id) waveWithEnemy:(EnemyTypes)enemy spawnRate:(float)spawnRate totalEnemies:(int)totalEnemies{
    
    return [[[self alloc] initWithEnemy:enemy spawnRate:spawnRate totalEnemies:totalEnemies] autorelease];
}

-(id) init{
    
    if((self = [super init])){
        
        m_currentEnemyIndex = 0;
        m_enemyType = BasicSealType;
        
    }
    
    return self;
}

//make static method?

-(id) initWithEnemy:(EnemyTypes)enemy spawnRate:(float)spawnRate totalEnemies:(int)totalEnemies{
    

    m_enemies = [[CCArray alloc] initWithCapacity:totalEnemies];
    
    if((self = [self init])){
        m_enemyType = enemy;
        m_totalEnemies = totalEnemies;
        m_spawnRate = spawnRate;
    
        if(enemy == BasicSealType){
            
            for(int i = 0; i < totalEnemies; i++){
            
            BasicSeal *seal = [BasicSeal enemyActor];
            [m_enemies addObject:seal];
            
            }
        }

        if(enemy == ArmorSealType){
            
            for(int i = 0; i < totalEnemies; i++){
                
                ArmorSeal *seal = [ArmorSeal enemyActor];
                [m_enemies addObject:seal];
                
            }
        }

    
    }
    
    return self;
}

-(EnemyActor *) getTarget{
    EnemyActor *enemy = nil;
    CCLOG(@"Inside Wave getTarget current index: %i", m_currentEnemyIndex);
    if(m_currentEnemyIndex < m_enemies.count-1){
        CCLOG(@"Inside Wave getTarget currentIndex: %i less Thatn m_enemies.count: %i", m_currentEnemyIndex, m_enemies.count);
        enemy = [m_enemies objectAtIndex:m_currentEnemyIndex];
        m_currentEnemyIndex++;
    }
    
    return enemy;
    
}

-(void) dealloc{
    
    //[m_enemyType dealloc];
  //  m_enemyType = nil;
    [m_enemies dealloc];
    m_enemies = nil;
    
    [super dealloc];
}

@end

























