//
//  Wave.h
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyActor.h"

@class EnemyActor;


@interface Wave : CCNode {
    
    float        m_spawnRate;
    int          m_totalEnemies;
    EnemyTypes  m_enemyType;
    int          m_currentEnemyIndex;
    CCArray     *m_enemies;
    
}

@property (nonatomic) float spawnRate;
@property (nonatomic) int totalEnemies, enemyIndex;
@property (nonatomic, assign) EnemyTypes enemyType;
@property (nonatomic, retain) CCArray *enemies;

+(id) waveWithEnemy:(EnemyTypes)enemy spawnRate:(float)spawnRate totalEnemies:(int) totalEnemies;

-(id) initWithEnemy:(EnemyTypes)enemy spawnRate:(float)spawnRate totalEnemies:(int) totalEnemies;

-(EnemyActor*) getTarget;

@end
