//
//  AppDelegate.h
//  Penguin
//
//  Created by Corey Schaf on 6/13/11.
//  Copyright EasyXP 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
