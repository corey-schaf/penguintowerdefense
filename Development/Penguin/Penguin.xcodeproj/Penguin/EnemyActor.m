//
//  EnemyActor.m
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "EnemyActor.h"


@implementation EnemyActor

@synthesize hp = m_curHp;
@synthesize moveDuration = m_moveDuration;
@synthesize curWaypoint = m_curWaypoint;

-(id) copyWithZone:(NSZone *)zone{
    EnemyActor *copy = [[[self class] allocWithZone:zone] initWithEnemy:self];
    return copy;
}

-(EnemyActor *) initWithEnemy:(EnemyActor *)copyFrom{
    if((self = [[[super alloc] initWithFile:@"Enemy1.png"] autorelease])){
        self.hp = copyFrom.hp;
        self.moveDuration = copyFrom.moveDuration;
        self.curWaypoint = copyFrom.curWaypoint;
    }
    [self retain];
    
    return self;
}

- (WayPoint *) getCurrentWaypoint{
	
	DataModel *m = [DataModel getModel];
	
    // get array of points
	WayPoint *waypoint = (WayPoint *) [m.waypoints objectAtIndex:self.curWaypoint];
	
	return waypoint;
}

- (WayPoint *) getNextWaypoint{
	
	DataModel *m = [DataModel getModel];
	int lastWaypoint = m.waypoints.count;
    
	self.curWaypoint++;
	
	if (self.curWaypoint > lastWaypoint)
		self.curWaypoint = lastWaypoint - 1;
	
	WayPoint *waypoint = (WayPoint *) [m.waypoints objectAtIndex:self.curWaypoint];
	
	return waypoint;
}


@end


@implementation BasicSeal

+(id)enemyActor{
    
    BasicSeal *enemy = nil;
    if((enemy = [[[super alloc] initWithFile:@"Enemy1.png"] autorelease])){
        enemy.hp = 10;
        enemy.moveDuration = 4;
		enemy.curWaypoint = 0;
    }
    
    return enemy;
}

@end



@implementation ArmorSeal

+(id)enemyActor{
    
    ArmorSeal *enemy = nil;
    if((enemy = [[[super alloc] initWithFile:@"Enemy2.png"] autorelease])){
        enemy.hp = 20;
        enemy.moveDuration = 10;
		enemy.curWaypoint = 0;
    }
    
    return enemy;
}

@end




