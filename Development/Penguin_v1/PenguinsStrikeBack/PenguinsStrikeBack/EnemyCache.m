//
//  EnemyCache.m
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 7/11/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "EnemyCache.h"
#import "EnemyActor.h"

@interface EnemyCache (PrivateMethods)
-(void) initEnemies;
@end


@implementation EnemyCache

@synthesize batch           = m_batch;
@synthesize enemeis         = m_enemies;

static EnemyCache *s_sharedContext = nil;

+(EnemyCache *) getCache{
    
    if(!s_sharedContext){
        s_sharedContext = [[self alloc] init];
    }
    
    return s_sharedContext;
}

+(id) cache{
    
    return [[[self alloc] init] autorelease];
}


// we build our 2D array of pre allocated enemy types
// this cache will serve as our cache for the game logic
-(void) initEnemies{
    
    int temp_capacity;
    
    m_enemies = [[CCArray alloc] initWithCapacity:EnemyTypeMAX];
    
    for(int i = 0; i < EnemyTypeMAX; i++){
        
        switch(i){
                
            case BasicSealType:
                temp_capacity = 30;
                break;
            
            case ArmorSealType:
                temp_capacity = 30;
                break;
                
                
            default:        [NSException exceptionWithName:@"EnemyCache Exception" reason:@"unhandled enemy type" userInfo:nil];
        }
        
        CCArray *temp_enemiesOfType = [CCArray arrayWithCapacity:temp_capacity];
        [m_enemies addObject:temp_enemiesOfType];
    }
    
    for(int i = 0; i < EnemyTypeMAX; i++){
        
        CCArray *enemiesOfType = [m_enemies objectAtIndex:i];
        int num = [enemiesOfType capacity];
        
        if(i == BasicSealType){
            
            for(int k = 0; k < num; k++){
                BasicSeal *_enemy = [BasicSeal enemyActor];
                //[m_batch addChild:_enemy z:0 tag:i];
                 [enemiesOfType addObject:_enemy];
            }
            
        }
        else if(i == ArmorSealType){
            
            for(int j = 0; j < num; j++){
                ArmorSeal *_enemy = [m_enemies objectAtIndex:i];
                //[m_batch addChild:_enemy z:0 tag:i];
                [enemiesOfType addObject:_enemy];
            }
            
        }
        
        
     
        
    }
    
}

-(EnemyActor *)spawn:(EnemyTypes)type{
    
    EnemyActor *enemy = nil;
    CCArray *cache = [m_enemies objectAtIndex:type];
    
    CCARRAY_FOREACH(cache, enemy){
        
        if(enemy.active == NO){
            enemy.active = YES;
            return enemy;
        }
        
    }
    
    return nil;

}

-(EnemyActor *) resetEnemy:(EnemyActor *)enemy{
    
    enemy.active = NO;
    
    return enemy;
}
    


-(void) dealloc
{
	[m_enemies release];
	[super dealloc];
}


@end






















