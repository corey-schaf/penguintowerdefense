//
//  HUDLayer.h
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 6/29/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HUDLayer : CCLayer {
    
    CCSprite        *m_background;
    CCSprite        *m_selSpriteRange;
    CCSprite        *m_selSprite;
    
    NSMutableArray  *m_movableSprites;
    
    CCLayerColor        *m_pauseLayer;
    CCMenu              *m_pauseMenu;
}

+(HUDLayer *) sharedHUD;
+(CGPoint) locationFromTouch:(UITouch*)touch;
@end
