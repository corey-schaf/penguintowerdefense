//
//  MenuScene.m
//  Penguin
//
//  Created by Corey Schaf on 6/13/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "MenuScene.h"
#import "SceneManager.h"

@implementation MenuScene

static MenuScene* instanceOfMenuScene;

+(MenuScene *) sharedMenuScene{
    NSAssert(instanceOfMenuScene != nil, @"MenuScene instance not yet initialized!");
	return instanceOfMenuScene;

}

+(id) scene{
    
	CCScene *scene = [CCScene node];
	
	MenuScene *layer = [MenuScene node];
    
	
	[scene addChild:layer];
	
	return scene;
}

-(id) init{
    
	if( (self = [super init] ) ){
		// init everyting here, buttons etc...
		
        
		CGSize winSize = [CCDirector sharedDirector].winSize;
		
        [self addChild:[MenuLayer node] z:1];
        instanceOfMenuScene = self;
		
		
	}
	return self;
}




-(void) dealloc{
	[super dealloc];
}

@end


@implementation MenuLayer

// <summary>
//
// Method: init
//
// Purpose: init MenuLayer
//
// </summary>

-(id) init{
	//self = [super init];
	if( self = [super init] ){
        
		CGSize winSize = [CCDirector sharedDirector].winSize;
        
        CCLayerColor *colorLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)];
		[self addChild:colorLayer z:0];
        m_fontQuickplay = [CCMenuItemFont itemFromString:@"QuickPlay" target:self selector:@selector(startGame:)];
        m_fontCredits = [CCMenuItemFont itemFromString:@"Credits" target:self selector:@selector(about:)];
        m_fontStats = [CCMenuItemFont itemFromString:@"Stats" target:self selector:@selector(stats:)];
		
		m_menu = [CCMenu menuWithItems:m_fontQuickplay, m_fontCredits, m_fontStats, nil];
		[m_menu alignItemsVertically];
        [m_menu setColor:ccc3(0, 0, 0)];
		[m_menu setPosition:ccp(winSize.width/2 + 100, winSize.height/2)];
        
        CCSprite *bg = [CCSprite spriteWithFile:@"menu_main.png"];
		//bg.scale = .50;
		//bg.position = ccp(0, 0);
		[bg setPosition:ccp(winSize.width/2, winSize.height/2)];
        //bg.anchorPoint = CGPointMake(0, 1);
        [self addChild:bg z:1];
        
		[self addChild:m_menu z:2];
		
	}
	return self;
	
}


// <summary>
//
// Method: startGame
//
// Purpose: selector sent to the menu option to start game scene
//          and game logic
//
// </summary>
-(void)startGame:(id) sender
{
	[SceneManager gotoQuickPlay];
	
}

// <summary>
//
// Method: about
//
// Purpose: selector sent to menu option to replace scene with credits
//
// </summary>
-(void) about:(id)sender{
	
    [SceneManager gotoCredits];
	
}

// <summary>
//
// Method: stats
//
// Purpose: selector to replace scene with stats layer + scene
//
// </summary>
-(void) stats:(id)sender{
    
    [SceneManager gotoStats];

}



-(void) dealloc{
    
    
    
    //[m_fontStats dealloc];
    m_fontStats = nil;
    
   //[m_fontQuickplay dealloc];
    m_fontQuickplay = nil;
    
    //[m_fontCredits dealloc];
    m_fontCredits = nil;
    
    //[m_menu dealloc];
    m_menu = nil;
    
    [super dealloc];
}


@end














