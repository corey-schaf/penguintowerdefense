//
//  EnemyCache.h
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 7/11/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyActor.h"

#define ENEMY_MAX_ARRAY 2



@interface EnemyCache : CCNode {
    
    CCSpriteBatchNode       *m_batch;
    CCArray                 *m_enemies;
    
    int updateCount;
}


@property (retain, nonatomic) CCSpriteBatchNode *batch;
@property (retain, nonatomic) CCArray *enemeis;

+(EnemyCache *) getCache;

-(void) initEnemies;
-(void) spawnEnemyOfType:(EnemyTypes)enemyType;
-(EnemyActor *) spawn:(EnemyTypes)type;
-(EnemyActor *) resetEnemy:(EnemyActor *)enemy;

@end
