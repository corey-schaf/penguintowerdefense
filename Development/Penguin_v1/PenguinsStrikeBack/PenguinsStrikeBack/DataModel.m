//
//  DataModel.m
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "DataModel.h"

@implementation DataModel

@synthesize level               = m_tileMapLevel;
@synthesize backgroundLayer     = m_backgroundLayer;
@synthesize gameLayer           = m_gameLayer;
@synthesize targets             = m_targets;
@synthesize waves               = m_waves;
@synthesize waypoints           = m_waypoints;
@synthesize gestureRecognizer   = m_gestureRecognizer;
@synthesize defenseUnits        = m_defenseUnits;
@synthesize HUDLayer            = m_gameHUDLayer;
@synthesize currentLevel        = m_currentLevel;
@synthesize viewController      = m_viewController;
@synthesize enemyBatch          = m_enemyBatchNode;
//@synthesize enemyCache          = m_enemyCache;
@synthesize totalScore          = m_totalScore;
@synthesize resources           = m_resources;
@synthesize isPaused            = m_isPaused;

static DataModel *s_sharedContext = nil;

+(DataModel*) getModel{
    if(!s_sharedContext){
        s_sharedContext = [[self alloc] init];
    }
    return s_sharedContext;
}


-(void)encodeWithCoder:(NSCoder *)coder {
    
}

-(id)initWithCoder:(NSCoder *)coder {
    
	return self;
}

- (id) init
{
	if ((self = [super init])) {
		m_targets = [[NSMutableArray alloc] init];
		
        m_waypoints = [[NSMutableArray alloc] init];
		
		m_waves = [[NSMutableArray alloc] init];
        
        m_defenseUnits = [[NSMutableArray alloc] init];
        
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
        m_enemyBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"enemy_sheet.pvr.ccz"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"enemy_sheet.plist"];
        
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_Default];
        
        m_isPaused = NO;
        m_gameState = NOTPLAYING;
	}
	return self;
}

- (BOOL) loadLevel:(Levels)level{
    
    NSAssert(level > 0, @"Error Loading Level");
    m_gameState = INTRO;
    
    return YES;
}

-(void)setGameState:(GameState)newState{
    //[[[CCDirector sharedDirector] openGLView] addGestureRecognizer:gestureRecognizer];
    if(newState == PAUSE){
        [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:m_gestureRecognizer]; 
    }
    if(newState == PLAYING){
        [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:m_gestureRecognizer];
    }
}

- (void) dealloc {	
	self.gameLayer = nil;
	self.gestureRecognizer = nil;
    self.HUDLayer = nil;
	
	[m_targets release];
	m_targets = nil;	
	
	[m_waypoints release];
	m_waypoints = nil;
	
	[m_waves release];
	 m_waves = nil;	
    
    [m_defenseUnits release];
    m_defenseUnits = nil;
    
    //[self.level dealloc];
    self.level = nil;
    
    //[self.backgroundLayer dealloc];
    self.backgroundLayer = nil;
    
	[super dealloc];
}

@end
