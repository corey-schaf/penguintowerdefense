//
//  GameScene.h
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    
    GameSceneLevel = 1,
    GameSceneLayer,
    GameSceneEnemy,
    
} GameSceneTags;

@interface GameScene : CCLayer {
    
    // move to specific game layer later on
    //and move all to LEVEL class for easy swapping
    CCTMXTiledMap       *m_tileMapLevel;
    CCTMXLayer          *m_backgroundLayer;
    int                 m_currentLevel;
    
    NSString            *m_objectLayerName;
    
    
}

@property (nonatomic, retain) CCTMXTiledMap *level;
@property (nonatomic, retain) CCTMXLayer    *backgroundLayer;
@property (nonatomic, assign) int currentLevel;
@property (nonatomic, retain) NSString *objects;

+(id) scene;
-(void) addWayPoint;
-(void) addWaves;
-(void) addTarget;
-(void) followPath:(id)sender;

@end
