//
//  GameScene.h
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HUDLayer.h"

typedef enum{
    
    GameSceneLevel = 1,
    GameSceneLayer,
    GameSceneEnemy,
    GameSceneDefense,
    GameSceneHUDLayer,
    GameObjectPause,
    
} GameSceneTags;



@interface GameScene : CCLayer {
    
    // move to specific game layer later on
    //and move all to LEVEL class for easy swapping
    CCTMXTiledMap       *m_tileMapLevel;
    CCTMXLayer          *m_backgroundLayer;
    int                 m_currentLevel;
    int                 m_maxWaves;
    
    NSString            *m_objectLayerName;
    
    HUDLayer            *m_gameHUD;
    BOOL                m_isPaused;
    CCLayerColor        *m_pauseLayer;
    CCMenu              *m_pauseMenu;
}

@property (nonatomic, retain) CCTMXTiledMap *level;
@property (nonatomic, retain) CCTMXLayer    *backgroundLayer;
@property (nonatomic, assign) int currentLevel;
@property (nonatomic, retain) NSString *objects;

+(id) scene;
-(void) addWaypoint;
-(void) addWaves;
-(void) addTarget;
-(void) followPath:(id)sender;

-(void) addDefense:(CGPoint) pos;
-(BOOL) canBuildOnTilePosition:(CGPoint) pos;


@end
