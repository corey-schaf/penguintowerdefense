//
//  StatsLayer.m
//  Penguin
//
//  Created by Corey Schaf on 6/13/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "StatsLayer.h"
#import "SceneManager.h"

@implementation StatsLayer

-(id) init{
    self = [super init];
    if( self != nil){
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        _backToMenu = [CCMenuItemFont itemFromString:@"back" target:self selector:@selector(back:)];
        
        CCMenu *_menu = [CCMenu menuWithItems:_backToMenu, nil];
        [_menu alignItemsVertically];
        [_menu setPosition:ccp(winSize.width/2 + 100, winSize.height/2)];
        
        [self addChild:_menu];
    }
    
    return self;
}

-(void) dealloc{
    //[_backToMenu dealloc];
    _backToMenu = nil;
    [super dealloc];
}

-(void) back:(id) sender{
    
    [SceneManager gotoMenu];
    
}    

@end
