//
//  HUDLayer.m
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 6/29/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "HUDLayer.h"
#import "DataModel.h"

@interface HUDLayer ()

-(void)pauseGame;
-(bool) isTouchForPause:(CGPoint)touchLocation;
-(bool) isTouchForPause:(CGPoint)touchLocation;

@end

@implementation HUDLayer

static HUDLayer *s_sharedHUD = nil;

+(HUDLayer *) sharedHUD{
    
    @synchronized( [HUDLayer class] ){
        
        if( !s_sharedHUD){
            [[self alloc] init];
        }
        
        return s_sharedHUD;
    }
    
    return nil;
}

+(CGPoint) locationFromTouch:(UITouch*)touch
{
	CGPoint touchLocation = [touch locationInView: [touch view]];
	return [[CCDirector sharedDirector] convertToGL:touchLocation];
}

+(id) alloc{
    
    @synchronized( [HUDLayer class] ){
        NSAssert(s_sharedHUD == nil, @"Attempted to allocate a second instance of a singleton, HUDLayer");
        
        s_sharedHUD = [super alloc];
        return s_sharedHUD;
    }
    
    return nil;
}

-(id) init{
    
    if((self = [super init])){
        
        //once again, extract eairler?, nm
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGB565];
        m_background = [CCSprite spriteWithFile:@"hud.png"];
        m_background.anchorPoint = ccp(0, 0);
        [self addChild:m_background];
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_Default];
        
        
        m_movableSprites = [[NSMutableArray alloc] init];
        
        NSArray *images = [NSArray arrayWithObjects:@"MachineGunTurret.png", @"MachineGunTurret.png", nil];
        
        for(int i = 0; i < images.count; ++i){
            NSString *image = [images objectAtIndex:i];
            CCSprite *sprite = [CCSprite spriteWithFile:image];
            float offsetFraction = ((float)(i+1))/(images.count +1);
            sprite.position = ccp(winSize.width * offsetFraction, 35);
                                            
            [self addChild:sprite];
            [m_movableSprites addObject:sprite];
            
            
        }
        
        [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
        
        //CCMenuItemFont *p
        CCSprite *pause = [CCSprite spriteWithFile:@"pause.png"];
        pause.position = CGPointMake(0, winSize.height);
        pause.anchorPoint = CGPointMake(0, 1);
        
        [self addChild:pause z:1 tag:GameObjectPause];
        
    }
    
    return self;
}

-(bool) isTouchForPause:(CGPoint)touchLocation
{
	CCNode* node = [self getChildByTag:GameObjectPause];
	return CGRectContainsPoint([node boundingBox], touchLocation);
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event{
    
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    DataModel *m = [DataModel getModel];
	CCSprite * newSprite = nil;
    
    for (CCSprite *sprite in m_movableSprites) {
        if (CGRectContainsPoint(sprite.boundingBox, touchLocation)) {  
			
			m.gestureRecognizer.enabled = NO;
			
			m_selSpriteRange = [CCSprite spriteWithFile:@"Range.png"];
			m_selSpriteRange.scale = 4;
			[self addChild:m_selSpriteRange z:-1];
			m_selSpriteRange.position = sprite.position;
			
            newSprite = [CCSprite spriteWithTexture:[sprite texture]]; //sprite;
			newSprite.position = sprite.position;
			m_selSprite = newSprite;
			[self addChild:newSprite];
			
            break;
        }
    }    
    
    CGPoint location = [HUDLayer locationFromTouch:touch];
    CCLOG(@"getting hud location");
    bool isTouchHandled = [self isTouchForPause:location];
    CCLOG(@"checking handled status");
    if(isTouchHandled){
        CCLOG(@"IS HANDLED CORRECTLY");
        //[self schedule:@selector(pauseGame:)];
        
        [self pauseGame];
    }

    

    //return TRUE;
    return YES;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event{
    
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    
    CGPoint oldTouchLocation = [touch previousLocationInView:touch.view];
    
    oldTouchLocation = [[CCDirector sharedDirector] convertToGL:oldTouchLocation];
    oldTouchLocation = [self convertToNodeSpace:oldTouchLocation];
    
    CGPoint translation = ccpSub(touchLocation, oldTouchLocation);
    
    if(m_selSprite){
        CGPoint newPos = ccpAdd(m_selSprite.position, translation);
        m_selSprite.position = newPos;
        m_selSpriteRange.position = newPos;
        
        DataModel *m = [DataModel getModel];
        CGPoint touchLocationInGameLayer = [m.gameLayer convertTouchToNodeSpace:touch];
        
        BOOL isBuildable = [m.gameLayer canBuildOnTilePosition: touchLocationInGameLayer];
        
        
        if(isBuildable){
            m_selSprite.opacity = 200;
            
        }else{
            m_selSprite.opacity = 50;
        }
        
        
        
        
    }
    
    
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event{
    
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    
    DataModel *m = [DataModel getModel];
    
    if(m_selSprite){
        
        CGRect backgroundRect = CGRectMake(m_background.position.x, m_background.position.y, m_background.contentSize.width, m_background.contentSize.height);
        
        if(!CGRectContainsPoint(backgroundRect, touchLocation)){
            
            CGPoint touchLocationInGameLayer = [m.gameLayer convertTouchToNodeSpace:touch];
            
            [m.gameLayer addDefense: touchLocationInGameLayer];
            
        }
        
        [self removeChild:m_selSprite cleanup:YES];
        m_selSprite = nil;
        
        [self removeChild:m_selSpriteRange cleanup:YES];
        m_selSpriteRange = nil;
    }
    
    m.gestureRecognizer.enabled = YES;
    
}

- (void) registerWithTouchDispatcher
{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (void) dealloc
{
	[m_movableSprites release];
    m_movableSprites = nil;
	[super dealloc];
}

-(void) pauseGame{

    DataModel *m = [DataModel getModel];
    
    //BOOL m_isPaused = ;
    
    if(![m isPaused]){
        
        m.isPaused = YES;
        
        [[CCDirector sharedDirector] pause];
        
        CGSize s = [[CCDirector sharedDirector] winSize];
        m_pauseLayer = [CCLayerColor layerWithColor:ccc4(150,150,150,125) width:s.width height:s.height];
        m_pauseLayer.position = CGPointZero;
        
        [self addChild:m_pauseLayer z:3];
        
        CCMenuItemFont *resume = [CCMenuItemFont itemFromString:@"Resume"target:self selector:@selector(resumeGame:)];
        CCMenuItemFont *quit = [CCMenuItemFont itemFromString:@"Quit"target:self selector:@selector(quitGame:)];
        
        
        m_pauseMenu = [CCMenu menuWithItems:resume, quit, nil ];
        m_pauseMenu.position = ccp(s.width / 2, s.height / 2);
        [m_pauseMenu alignItemsVertically];
        
        [self addChild:m_pauseMenu z:10];
        
        //m.isPaused = m_isPaused;
    }
}

-(void)resumeGame:(id)sender{
    
    DataModel *m = [DataModel getModel];
    
    BOOL m_isPaused = [m isPaused];

    [self removeChild:m_pauseMenu cleanup:YES];
    [self removeChild:m_pauseLayer cleanup:YES];
    
    [[CCDirector sharedDirector] resume];
    
    m.isPaused = NO;
}

-(void)quitGame:(id)sender{
    
    
    
    
}

@end


















