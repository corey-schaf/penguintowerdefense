//
//  Wave.h
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyActor.h"

@interface Wave : CCNode {
    
    float        m_spawnRate;
    int          m_totalEnemies;
    EnemyActor  *m_enemyType;
    
}

@property (nonatomic) float spawnRate;
@property (nonatomic) int totalEnemies;
@property (nonatomic, retain) EnemyActor *enemyType;

-(id) initWithEnemy:(EnemyActor *)enemy spawnRate:(float)spawnRate totalEnemies:(int) totalEnemies;

@end
