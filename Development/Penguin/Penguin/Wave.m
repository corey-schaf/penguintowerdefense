//
//  Wave.m
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "Wave.h"


@implementation Wave


@synthesize spawnRate = m_spawnRate;
@synthesize totalEnemies = m_totalEnemies;
@synthesize enemyType = m_enemyType;

-(id) init{
    
    if((self = [super init])){
        
    }
    
    return self;
}

-(id) initWithEnemy:(EnemyActor *)enemy spawnRate:(float)spawnRate totalEnemies:(int)totalEnemies{
    
    NSAssert(enemy != nil, @"Invalid enemy for wave");
    
    if((self = [self init])){
        m_enemyType = enemy;
        m_totalEnemies = totalEnemies;
        m_spawnRate = spawnRate;
    }
    
    return self;
}

-(void) dealloc{
    
    [m_enemyType dealloc];
    m_enemyType = nil;
    
    [super dealloc];
}

@end
