//
//  DataModel.m
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "DataModel.h"

@implementation DataModel


@synthesize gameLayer = m_gameLayer;
@synthesize targets = m_targets;
@synthesize waves = m_waves;
@synthesize waypoints = m_waypoints;
@synthesize gestureRecognizer = m_gestureRecognizer;

static DataModel *s_sharedContext;

+(DataModel*) getModel{
    if(!s_sharedContext){
        s_sharedContext = [[self alloc] init];
    }
    return s_sharedContext;
}


-(void)encodeWithCoder:(NSCoder *)coder {
    
}

-(id)initWithCoder:(NSCoder *)coder {
    
	return self;
}

- (id) init
{
	if ((self = [super init])) {
		m_targets = [[NSMutableArray alloc] init];
		
        m_waypoints = [[NSMutableArray alloc] init];
		
		m_waves = [[NSMutableArray alloc] init];
	}
	return self;
}

- (void)dealloc {	
	self.gameLayer = nil;
	self.gestureRecognizer = nil;
	
	[m_targets release];
	m_targets = nil;	
	
	[m_waypoints release];
	m_waypoints = nil;
	
	[m_waves release];
	 m_waves = nil;	
	[super dealloc];
}

@end
