//
//  SceneManager.m
//  Penguin
//
//  Created by Corey Schaf on 6/13/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "SceneManager.h"
#import "MenuScene.h"
#import "CreditsLayer.h"
#import "StatsLayer.h"


@implementation SceneManager

// <summary>
//
// Method: 
//
// Purpose: 
//
// Notes: DEPRECIATED 
//
// </summary>
+(MenuScene *) getMenuScene{
    
    return [MenuScene sharedMenuScene];
    
}

// <summary>
//
// Method: runMenuScene
//
// Purpose: reutrn instance of menu scene, our main menu
//
// </summary>
+(MenuScene*)runMenuScene{
    
    return [MenuScene scene];
}


// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>

+(GameScene*) runQPGameScene{
    return [GameScene scene];
}


// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
// This can be optimized if needed
+(CGSize) winSize{
    return [[CCDirector sharedDirector] winSize];
}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(CCScene *) wrap:(CCLayer *)layer{
    
    CCScene *newScene = [CCScene node];
    [newScene addChild: layer];
    
    return newScene;
}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(void) go: (CCLayer *) layer{
    
    CCDirector *_director = [CCDirector sharedDirector];
    
    CCScene *_newScene = [SceneManager wrap:layer];
    
    if ([_director runningScene]) {
        
        [_director replaceScene: _newScene];
        
    }else {
        
        [_director runWithScene:_newScene];
        
    }
    
}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(void) gotoQuickPlay{
    
    //[[CCDirector sharedDirector] replaceScene:[GameScene scene]];
    CCDirector *_director = [CCDirector sharedDirector];
    if( [_director runningScene]){
        
        [_director replaceScene:[SceneManager runQPGameScene]];
    }else{
        [_director runWithScene:[SceneManager runQPGameScene]];
    }

    
}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(void) gotoMenu{
    
    CCDirector *_director = [CCDirector sharedDirector];

    
    if( [_director runningScene] ){
       
        [_director replaceScene:[SceneManager runMenuScene]];
    }else{
        [_director runWithScene:[SceneManager runMenuScene]];
    }

}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(void) gotoCredits{
    CCLayer *_layer = [CreditsLayer node];
    [SceneManager go:_layer];
}

// <summary>
//
// Method: 
//
// Purpose: 
//
// </summary>
+(void) gotoStats{
    CCLayer *_layer = [StatsLayer node];
    [SceneManager go:_layer];
    
}

// <summary>
//
// Method: pushLayer
//
// Purpose: add layer to scene with tag
//
// </summary>
+(void) pushLayer:(CCLayer *)layer onto:(CCScene *)scene withTag:(NodeTags)tag{
    
    [[CCScene node] addChild:layer z:1 tag:tag];
    
}

// <summary>
//
// Method: popLayer
//
// Purpose: remove layer by tag from scene
//
// Notes: no need now, 6/14, possible use with game logic implementation
//
// </summary>
+(void) popLayer:(CCScene *)scene withTag:(NodeTags)tag{

    [scene removeChildByTag:tag cleanup:YES];
}
  
-(void) dealloc{
    [super dealloc];
}
  

@end






