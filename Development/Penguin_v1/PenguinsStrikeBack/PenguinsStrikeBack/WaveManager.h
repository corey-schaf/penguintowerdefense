//
//  WaveManager.h
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 7/9/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Wave.h"

@interface WaveManager : NSObject {
    
    CCArray         *m_waves;
    Wave            *m_currentWave;
    
    int             m_waveSpawnRate;
    int             m_timeSinceLastSpawn;       //TODO INIT
    int             m_currentWaveIndex;
    int             m_waveWaitTime;
    
    
    BOOL            m_waveCurrentlyRunning;
    BOOL            m_gameBegin;
    
    
    
}

@property (nonatomic, retain) CCArray *waves;
@property (nonatomic, assign) int waveIndex, waveWaitTime, spwanRate;
@property (nonatomic, assign) BOOL firstWave, running;

+(WaveManager *) getWaveManager;
-(void) initWavesWithLevel:(Levels) level;
-(void) addWaveToDataModel;
-(Wave *) getCurrentWave;
-(Wave *) getNextWave;
-(EnemyActor *) addTarget;
-(void) setCurrentWave:(Wave*)wave;

@end
