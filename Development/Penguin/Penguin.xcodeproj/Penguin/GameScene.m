//
//  GameScene.m
//  Penguin
//
//  Created by Corey Schaf on 6/27/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "GameScene.h"
#import "DataModel.h"
#import "WayPoint.h"
#import "Wave.h"

@implementation GameScene

//syn our properties
@synthesize level = m_tileMapLevel;
@synthesize backgroundLayer = m_backgroundLayer;
@synthesize currentLevel = m_currentLevel;
@synthesize objects = m_objectLayerName;

+(id) scene{
    
    CCScene *scene = [CCScene node];
    GameScene *layer = [GameScene node];
    [scene addChild:layer z:0 tag:GameSceneLayer];
    
    return scene;
}

-(id) init{
    
    if((self = [super init])){
        
        self.level = [CCTMXTiledMap tiledMapWithTMXFile:@"Level2.tmx"];
        self.backgroundLayer = [m_tileMapLevel layerNamed:@"Background"];
        self.backgroundLayer.anchorPoint = ccp(0, 0);
        [self addChild:m_tileMapLevel z:0 tag:GameSceneLevel];
        
        self.objects = @"Objects";
        
        // add waypoints from tile map
        //[self addWayPoint];
        //[self addWaves];
        
        // call game logic about every second
        [self schedule:@selector(update:)];
        [self schedule:@selector(gameLogic:) interval:1.0];
        
        //move to level manager singleton?
        self.currentLevel = 0;
        
        //self.position = ccp(-228, -122);
        
    }
    
    return self;
    
}

-(void)addWaves {
	DataModel *m = [DataModel getModel];
	
	Wave *wave = nil;
	wave = [[Wave alloc] initWithEnemy:[BasicSeal enemyActor] spawnRate:1.0 totalEnemies:20];
	[m.waves addObject:wave];
	wave = nil;
}

- (Wave *)getCurrentWave{
	
	DataModel *m = [DataModel getModel];	
	Wave * wave = (Wave *) [m.waves objectAtIndex:self.currentLevel];
	
	return wave;
}

- (Wave *)getNextWave{
	
	DataModel *m = [DataModel getModel];
	
	self.currentLevel++;
	
	if (self.currentLevel > 1)
		self.currentLevel = 0;
	
    Wave * wave = (Wave *) [m.waves objectAtIndex:self.currentLevel];
    
    return wave;
}

-(void)addWaypoint {
	DataModel *m = [DataModel getModel];
	
	CCTMXObjectGroup *objects = [self.level objectGroupNamed:@"Objects"];
	WayPoint *wp = nil;
	
	int spawnPointCounter = 0;
	NSMutableDictionary *spawnPoint;
	while ((spawnPoint = [objects objectNamed:[NSString stringWithFormat:@"Waypoint%d", spawnPointCounter]])) {
		int x = [[spawnPoint valueForKey:@"x"] intValue];
		int y = [[spawnPoint valueForKey:@"y"] intValue];
		
		wp = [WayPoint node];
		wp.position = ccp(x, y);
		[m.waypoints addObject:wp];
		spawnPointCounter++;
	}
	
	NSAssert([m.waypoints count] > 0, @"Waypoint objects missing");
	wp = nil;
}

-(void) addTarget{
    
    // TODO:
    // ADD REFERENCE TO CLASS TO AVOID REPEAT METHOD CALLS
    DataModel *m = [DataModel getModel];
    Wave *wave = [self getCurrentWave];
    
    if(wave.totalEnemies < 0){
        return;
    }
    
    EnemyActor *target = nil;
    
    //figure out how this method works
    if((arc4random() % 2) == 0){
        target = [BasicSeal enemyActor];
    }else{
        target = [ArmorSeal enemyActor];
    }
    
    
	WayPoint *waypoint = [target getCurrentWaypoint ];
	target.position = waypoint.position;	
	waypoint = [target getNextWaypoint ];

    //adding with this tag will return a collection of enemies
    [self addChild:target z:1 tag:GameSceneEnemy];
    
    int moveDuration = target.moveDuration;	
	id actionMove = [CCMoveTo actionWithDuration:moveDuration position:waypoint.position];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(followPath:)];
	[target runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
    //add our target to the array of objects
    target.tag = GameSceneEnemy;
    [m.targets addObject:target];
    
}

-(void) followPath:(id)sender{
    
    EnemyActor *enemy = (EnemyActor*)sender;
    WayPoint * waypoint = [enemy getNextWaypoint];
    
	int moveDuration = enemy.moveDuration;
	id actionMove = [CCMoveTo actionWithDuration:moveDuration position:waypoint.position];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(followPath:)];
	[enemy stopAllActions];
	[enemy runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

-(void) gameLogic:(ccTime)dt{
    
    //DataModel *m = [DataModel getModel];
    Wave *wave = [self getCurrentWave];
    static double lastTimeTargetAdded = 0;
    double now = [[NSDate date] timeIntervalSince1970];
    if(lastTimeTargetAdded == 0 || now - lastTimeTargetAdded >= wave.spawnRate) {
        [self addTarget];
        lastTimeTargetAdded = now;
    }
}


- (void)update:(ccTime)dt {
    
    CCLOG(@"inside gamelogic updated, not frozen yet");
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    //[self.objects dealloc];
    self.objects = nil;
    
    //[self.level dealloc];
    self.level = nil;
    
    //[self.backgroundLayer dealloc];
    self.backgroundLayer = nil;
    
    
    
	[super dealloc];
}


- (CGPoint)boundLayerPos:(CGPoint)newPos {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGPoint retval = newPos;
    retval.x = MIN(retval.x, 0);
    retval.x = MAX(retval.x, -m_tileMapLevel.contentSize.width+winSize.width); 
    retval.y = MIN(0, retval.y);
    retval.y = MAX(-m_tileMapLevel.contentSize.height+winSize.height, retval.y); 
    return retval;
}

- (void)handlePanFrom:(UIPanGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {    
        
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
        touchLocation = [self convertToNodeSpace:touchLocation];                
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {    
        
        CGPoint translation = [recognizer translationInView:recognizer.view];
        translation = ccp(translation.x, -translation.y);
        CGPoint newPos = ccpAdd(self.position, translation);
        self.position = [self boundLayerPos:newPos];  
        [recognizer setTranslation:CGPointZero inView:recognizer.view];    
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
		float scrollDuration = 0.2;
		CGPoint velocity = [recognizer velocityInView:recognizer.view];
		CGPoint newPos = ccpAdd(self.position, ccpMult(ccp(velocity.x, velocity.y * -1), scrollDuration));
		newPos = [self boundLayerPos:newPos];
        
		[self stopAllActions];
		CCMoveTo *moveTo = [CCMoveTo actionWithDuration:scrollDuration position:newPos];            
		[self runAction:[CCEaseOut actionWithAction:moveTo rate:1]];            
        
    }        
}

@end






















