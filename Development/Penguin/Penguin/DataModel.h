//
//  DataModel.h
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DataModel : NSObject <NSCoding> {
    
    CCLayer         *m_gameLayer;       //Pointer to actual game layer
    NSMutableArray  *m_targets;
    NSMutableArray  *m_waypoints;
    NSMutableArray  *m_waves;
    
    UIPanGestureRecognizer  *m_gestureRecognizer;
    
}

@property (nonatomic, retain) CCLayer *gameLayer;
@property (nonatomic, retain) NSMutableArray *targets;
@property (nonatomic, retain) NSMutableArray *waves;
@property (nonatomic, retain) NSMutableArray *waypoints;
@property (nonatomic, retain) UIPanGestureRecognizer *gestureRecognizer;

+(DataModel*) getModel;

@end
