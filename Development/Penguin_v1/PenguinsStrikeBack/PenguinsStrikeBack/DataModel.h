//
//  DataModel.h
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "RootViewController.h"
#import "GameScene.h"

//@class EnemyCache;
//@class GameScene;

typedef enum{
    ONE = 1,
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    SIX = 6,
    SEVEN = 7,
    EIGHT = 8,
    NINE = 9, 
    TEN = 10,
    ELEVEN = 11,
    BONUS1,
    
} Levels;

typedef enum{
    NOTPLAYING = 1,
    INTRO,
    PLAYING,
    PAUSE,
    QUIT,
    FAIL,
    SUCCESS,
    
} GameState;

@interface DataModel : NSObject <NSCoding> {
    
    CCTMXTiledMap       *m_tileMapLevel;
    CCTMXLayer          *m_backgroundLayer;
    
    CCLayer         *m_gameLayer;    //Pointer to actual game layer
    CCLayer         *m_gameHUDLayer;
    
    NSMutableArray  *m_targets;
    NSMutableArray  *m_waypoints;
    NSMutableArray  *m_waves;
    NSMutableArray  *m_defenseUnits;
    
    UIPanGestureRecognizer  *m_gestureRecognizer;
    
    int             m_currentLevel;
    int             m_totalScore;
    int             m_resources;
    
    //reference to controller
    //RootViewController *viewController;
    UIViewController *viewController;
    
    CCSpriteBatchNode *m_enemyBatchNode;
    
    // holds reference to enemy cache, init in gamescene?
   // EnemyCache      *m_enemyCache;
    BOOL              m_isPaused;
    GameState         m_gameState;
    
    
}

@property (nonatomic, retain) CCTMXTiledMap *level;
@property (nonatomic, retain) CCTMXLayer    *backgroundLayer;

@property (nonatomic, retain) CCLayer *gameLayer;
@property (nonatomic, retain) CCLayer *HUDLayer;

@property (nonatomic, retain) NSMutableArray *targets;
@property (nonatomic, retain) NSMutableArray *waves;
@property (nonatomic, retain) NSMutableArray *waypoints;
@property (nonatomic, retain) NSMutableArray *defenseUnits;

@property (nonatomic, assign) int currentLevel;
@property (nonatomic, assign) int totalScore;
@property (nonatomic, assign) int resources;

@property (nonatomic, retain) UIViewController *viewController;

@property (nonatomic, retain) UIPanGestureRecognizer *gestureRecognizer;

@property (readonly) CCSpriteBatchNode *enemyBatch;

@property (nonatomic, assign) BOOL isPaused;

@property (readonly)GameState GAME_STATE;

//@property (nonatomic, retain) EnemyCache *enemyCache;


+(DataModel*) getModel;

-(BOOL) loadLevel:(Levels) level;
-(BOOL) loadLevel:(Levels) level withScene:(GameScene *)scene;
-(void)setGameState:(GameState) newState;



@end
