//
//  DefenseEntity.h
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 6/28/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyActor.h"

@interface DefenseEntity : CCSprite {
    
    int         m_range;
    CCSprite    *m_selSpriteRange;
    
    EnemyActor  *m_target;
    
}

@property (nonatomic, assign) int range;
@property (nonatomic, retain) EnemyActor* target;

-(EnemyActor*) getClosestTarget;

@end




@interface IceTower : DefenseEntity {

}

+(id) tower;

-(void) towerLogic:(ccTime)dt;
-(void) setClosestTarget:(EnemyActor *)closestTarget;

@end
