//
//  DefenseEntity.m
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 6/28/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "DefenseEntity.h"
#import "WaveManager.h"

@implementation DefenseEntity

@synthesize range = m_range;
@synthesize target = m_target;

- (EnemyActor *)getClosestTarget {
	EnemyActor *closestEnemy = nil;
	double maxDistant = 99999;
	
	//DataModel *m = [DataModel getModel];
	
    WaveManager *w = [WaveManager getWaveManager];
    
	for (CCSprite *target in [w getCurrentWave].enemies ) {	
		EnemyActor *enemy = (EnemyActor *)target;
		double curDistance = ccpDistance(self.position, enemy.position);
		
		if (curDistance < maxDistant) {
			closestEnemy = enemy;
			maxDistant = curDistance;
		}
		
	}
	
	if (maxDistant < self.range)
		return closestEnemy;
	
	return nil;
}

@end

@implementation IceTower

+(id) tower{
    
    IceTower *tower = nil;
    if((tower = [[[super alloc] initWithFile:@"MachineGunTurret.png"] autorelease])){
        
        tower.range = 200;
        
        tower.target = nil;
        
        [tower schedule:@selector(towerLogic:) interval:0.2];
    }
    
    return tower;
    
}

-(id) init{
    
    if((self = [super init])){
        
    }
    
    return self;
}

-(void)setClosestTarget:(EnemyActor *)closestTarget {
	self.target = closestTarget;
}

-(void) towerLogic:(ccTime)dt{
    
    
	self.target = [self getClosestTarget];
	
	if (self.target != nil) {
        
		//rotate the tower to face the nearest creep
		CGPoint shootVector = ccpSub(self.target.position, self.position);
		CGFloat shootAngle = ccpToAngle(shootVector);
		CGFloat cocosAngle = CC_RADIANS_TO_DEGREES(-1 * shootAngle);
		
		float rotateSpeed = 0.5 / M_PI; // 1/2 second to roate 180 degrees
		float rotateDuration = fabs(shootAngle * rotateSpeed);    
		
        
        /*
         
         CGFloat curAngle = self.rotation;
         CGFloat rotateDiff = cocosAngle - curAngle;
         if (rotateDiff > 180)
            rotateDiff -= 360;
         if (rotateDiff < -180)
            rotateDiff += 360;
         
         CGFloat rotateSpeed = 0.5 / 180; // Would take 0.5 seconds to rotate half a circle
         
         CGFloat rotateDuration = fabs(rotateDiff * rotateSpeed);
         
         

         
         */
        
        
		[self runAction:[CCSequence actions:
                         [CCRotateTo actionWithDuration:rotateDuration angle:cocosAngle],
                         nil]];		
	}

}

@end










