//
//  EnemyActor.h
//  Penguin
//
//  Created by Corey Schaf on 6/22/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModel.h"
#import "WayPoint.h"


typedef enum{
    
    BasicSealType = 0,
    ArmorSealType,
    EnemyTypeMAX
    
} EnemyTypes;

@interface EnemyActor : CCSprite <NSCopying> {
    
    int     m_curHp;
    int     m_moveDuration;
    
    int     m_curWaypoint;
    BOOL    m_active;
    
}


@property (nonatomic, assign) int hp;
@property (nonatomic, assign) int moveDuration;
@property (nonatomic, assign) int curWaypoint;
@property (nonatomic, assign) BOOL active;


-(EnemyActor *) initWithEnemy:(EnemyActor*) copyFrom;
-(WayPoint *) getCurrentWaypoint;
-(WayPoint *) getNextWaypoint;

@end

// Our basic seal, insitu for now, possible
// to move if class needs more differentiation
@interface BasicSeal : EnemyActor {
    
}
+(id) enemyActor;
@end

@interface ArmorSeal : EnemyActor {

}
+(id) enemyActor;
@end
    
    

