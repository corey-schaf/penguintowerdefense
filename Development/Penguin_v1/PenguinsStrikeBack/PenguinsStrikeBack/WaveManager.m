//
//  WaveManager.m
//  PenguinsStrikeBack
//
//  Created by Corey Schaf on 7/9/11.
//  Copyright 2011 EasyXP. All rights reserved.
//

#import "WaveManager.h"
#import "DataModel.h"
#import "EnemyCache.h"

#define MAX_WAVES  30

@implementation WaveManager

@synthesize waves           = m_waves;
@synthesize waveIndex       = m_currentWaveIndex;
@synthesize waveWaitTime    = m_waveWaitTime;
@synthesize firstWave       = m_gameBegin;
@synthesize spwanRate       = m_spawnRate;
@synthesize running         = m_waveCurrentlyRunning;

static WaveManager *s_sharedContext = nil;

+(WaveManager *) getWaveManager{
    
    if(!s_sharedContext){
        s_sharedContext = [[self alloc] init];
    }
    
    return s_sharedContext;
}

-(id) init{
    
    if((self = [super init])){
        
        // allocate mem for our array that holds waves
        m_waves = [[CCArray alloc] initWithCapacity:MAX_WAVES];
        m_currentWaveIndex = 0;
        m_gameBegin = YES;
    }
    
    return self;
}

-(void) addWaveToDataModel{
    
}

-(Wave *) getCurrentWave{
      CCLOG(@"In WaveManager getCurrentWave: %i", m_currentWaveIndex);
    //Wave *_wave = nil;
    // should never be less then 0
    if(m_currentWaveIndex >= 0){
          CCLOG(@"Inside getCurrentWaveLoop Wave#: %i", m_currentWaveIndex);
        m_currentWave = [m_waves objectAtIndex:m_currentWaveIndex];
        
    }
    
    return m_currentWave;
    
}
-(void) setCurrentWave:(Wave*)wave{
    
    if(wave != nil){
        
        m_currentWave = wave;
    }
}

-(Wave *) getNextWave{
    
    CCLOG(@"Get NExt Wave Number: %i", m_currentWaveIndex);
    
    Wave *wave = nil;
    
    if(m_gameBegin){
        CCLOG(@"Inside WaveManager getNextWave ifStatment: %i", m_currentWaveIndex);    
        wave = [m_waves objectAtIndex:0];
        m_gameBegin = NO;
        m_waveCurrentlyRunning = YES;
    }
    
    else{
        
        CCLOG(@"Inside WaveManager getNextWave elseStatment currentIndex: %i", m_currentWaveIndex);
        ++m_currentWaveIndex;
        CCLOG(@"Inside WaveManager getNextWave elseStatment currentIndex now : %i", m_currentWaveIndex);
        if(m_currentWaveIndex < m_waves.count-1){
            
            CCLOG(@"Inside WaveManager getNextWave currentWaveIndex: %i  totalWaves.count: %i", m_currentWaveIndex, m_waves.count);
            wave = [m_waves objectAtIndex:m_currentWaveIndex];
        
        }else{
            m_waveCurrentlyRunning = NO;
        }
    }
    
    return wave;
}

-(void) dealloc{
    
    [m_waves release];
    [super dealloc];
}

-(void) initWavesWithLevel:(Levels)level{
    
    switch (level){
            
        case ONE:
            
            m_waves = [[CCArray alloc] initWithCapacity:25];
            for(int i = 0; i < 20; i++){
                
                Wave *wave = [Wave waveWithEnemy:BasicSealType spawnRate:1 totalEnemies:10];
                wave.enemyType = BasicSealType;
                [m_waves addObject:wave];
            }
            for(int j = 20; j < 25; j++){
                
                Wave *wave = [Wave waveWithEnemy:ArmorSealType spawnRate:2 totalEnemies:5];
                wave.enemyType = ArmorSealType;
                [m_waves addObject:wave];

            }
            
            break;
            
        case TWO:
            
            break;
            
        case THREE:
            
            break;
            
        case FOUR:
            
            break;
            
        case FIVE:
            
            break;
            
        case SIX:
            
            break;
         
        case SEVEN:
            
            break;
 
        case EIGHT:
            
            break;
 
        case NINE:
            
            break;
 
        case TEN:
            
            break;
 
        case ELEVEN:
            
            break;
 
        case BONUS1:
            
            break;
            
        default:
            break;
    }
}


-(EnemyActor *) addTarget{
    
    EnemyActor *enemy = nil;
    EnemyCache *cache = [EnemyCache getCache];
    
    int index = m_currentWave.enemyIndex;
    
    if(index <= m_currentWave.totalEnemies){
        
        enemy = [cache spawn:m_currentWave.enemyType];
        
    }
    
}


@end
































